import 'dart:io';
import 'Recommended menu.dart';
import 'package:taobin/taobin.dart' as taobin;
import 'coffee.dart';
import 'tea.dart';
import 'milk.dart';
import 'protein.dart';
import 'soda.dart';

void main(List<String> arguments) {
  var recomm = new RecommendedMenu();
  var coffee = new coffe();
  var tea = new teaa();
  var milk = new milkk();
  var protein = new proteinn();
var soda = new sodaa();
  int choice = 0;
  int choiceMenu = 0;
  String message =
      '''
  Select menu category
  1. เมนูแนะนำ
  2. กาแฟ
  3. ชา
  4. นม โกโก้และคาราเมล
  5. โปรตีนเชค
  6. น้ำโซดาและอื่นๆ
   ''';
  stdout.write(message);
  print("Input your Choice : ");
  choice = int.parse(stdin.readLineSync()!);

  if (choice == 1) {
    String m =
        '''
  เลือกเมนู
  1. สตอเบอรี่ปั่น 45 บาท
  2. โกโก้สตอเบอรรี่ปั่น 45 บาท
  3. โอริโอ้ปั่นภูเขาไฟ 55 บาท
  4. มัทฉะลาเต้ปั่น 50 บาท
  5. โกโก้เย็นปั่น 40 บาท 
   ''';
    stdout.write(m);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
      recomm.Stbmilksh();
    }
    if (choiceMenu == 2) {
      recomm.StrawberryCocoaSmoothie();
    }
    if (choiceMenu == 3) {
      recomm.VolcanoSpinningOreo();
    }
    if (choiceMenu == 4) {
      recomm.MatchaLatteSmoothie();
    }
    if (choiceMenu == 5) {
      recomm.IcedCocoaShake();
    }
  }
  if (choice == 2) {
    String a =
        '''
  เลือกเมนู
  1. ลาเต้ร้อน 35 บาท
  2. คาปูชิโน่ร้อน 35 บาท
  3. คาราเมลลาเต้ร้อน 45 บาท
  4. เอรสเพรสโซ่เย็น 45 บาท
  5. มอคค่าร้อน 40 บาท
   ''';
    stdout.write(a);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
      coffee.hotlatte();
    }
    if (choiceMenu == 2) {
      coffee.hotcappuccino();
    }
    if (choiceMenu == 3) {
      coffee.HotCaramelLatte();
    }
    if (choiceMenu == 4) {
      coffee.IcedEspresso();
    }
    if (choiceMenu == 5) {
      coffee.hotmocha();
    }
  }
  if (choice == 3) {
    String b =
        '''
  เลือกเมนู
  1. เก๊กฮวยร้อน 20 บาท
  2. ชาขิงร้อน 20 บาท
  3. ชามะนาวร้อน 20 บาท
  4. ชาลิ้นจี่ร้อน 25 บาท 
  5. ชาไทยร้อน 35 บาท
   ''';
    stdout.write(b);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
      tea.hotchrysanthemum();
    }
    if (choiceMenu == 2) {
      tea.hotgingertea();
    }
    if (choiceMenu == 3) {
      tea.hotlemontea();
    }
    if (choiceMenu == 4) {
      tea.hotlycheetea();
    }
    if (choiceMenu == 5) {
      tea.hotthaitea();
    }
  }
  if (choice == 4) {
    String v =
        '''
  เลือกเมนู
  1. นมคาราเมลร้อน 35 บาท
  2. โกโก้ร้อน 35 บาท
  3. โกโก้เย็น 35 บาท
  4. นมร้อน 30 บาท
  5. นมเย็น 30 บาท
   ''';
    stdout.write(v);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
      milk.hotcaramelmilk();
    }
    if (choiceMenu == 2) {
      milk.hotcocoa();
    }
    if (choiceMenu == 3) {
      milk.coldcocoa();
    }
    if (choiceMenu == 4) {
      milk.hotmilk();
    }
    if (choiceMenu == 5) {
      milk.coldmilk();
    }
  }
  if (choice == 5) {
    String v =
        '''
  เลือกเมนู
  1. มัทฉะโปรตีน 60 บาท
  2. โกโก้โปรตีน 60 บาท
  3. สตอเบอรรี่โปรตีน 60 บาท
  4. ชาใต้หวันโปรตีน 60 บาท
  5. คาราเมลโปรตีน 60 บาท
   ''';
    stdout.write(v);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
protein.matchaprotein();
    }
    if (choiceMenu == 2) {
protein.CocoaProtein();
    }
    if (choiceMenu == 3) {
protein.Strawberryprotein();
    }
    if (choiceMenu == 4) {
protein.Taiwaneseteaprotein();
    }
    if (choiceMenu == 5) {
protein.CaramelProtein();
    }
  }
  if (choice == 6) {
    String g =
        '''
  เลือกเมนู
  1. ลิ้นจี่โซดา 25 บาท
  2. มะนาวโซดา 20 บาท
  3. แดงโซดา 20 บาท
  4. กัญชาโซดา 40 บาท
  5. ขิงโซดา 25 บาท
   ''';
    stdout.write(g);
    print("Input your menu Choice : ");
    choiceMenu = int.parse(stdin.readLineSync()!);
    if (choiceMenu == 1) {
      soda.LycheeSoda();
    }
    if (choiceMenu == 2) {
      soda.Limesoda();
    }
    if (choiceMenu == 3) {
      soda.RedSoda();
    }
    if (choiceMenu == 4) {
      soda.CannabisSoda();
    }
    if (choiceMenu == 5) {
      soda.Gingersoda();
    }
  }
  if (choice > 6 || choice < 0) {
    print("Invalid Choice");
  }
}
